﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            using(var context = new TestConversionContext())
            {
                context.Test.ToList().ForEach(x => Console.WriteLine(x.TestField));

                context.Add(new Test
                {
                    TestField = 99.1m
                });
                context.SaveChanges();
            }

            Console.WriteLine("Done!");
        }
    }
}
