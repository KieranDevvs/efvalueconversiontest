# EfValueConversionTest

1) Create a local database called `TestConversion` and then run the script `CreateTableScript.sql` against it.
2) Run the value conversion ef core console app (this will add a record)
3) Run it once more and see that the console reads one type from the database and converts it to another.