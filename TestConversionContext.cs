﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ConsoleApp2
{
    public partial class TestConversionContext : DbContext
    {
        public virtual DbSet<Test> Test { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=TestConversion;Integrated Security=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var converter = new ValueConverter<decimal, double>(
                v => (double)v,
                v => (decimal)v
            );

            modelBuilder
                .Entity<Test>()
                .Property(e => e.TestField)
                .HasConversion(converter);
        }
    }
}
